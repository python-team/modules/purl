purl (1.6-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Michael Fladischer ]
  * New upstream release.
  * Bump Standards-Version to 4.6.0.
  * Use uscan version 4.
  * Bump debhelper version to 13.
  * Set Rules-Requires-Root: no.
  * Remove unnecessary autopkgtest-pkg-python testsuite.
  * Fix missing newline in d/watch.

 -- Michael Fladischer <fladi@debian.org>  Sat, 28 Aug 2021 20:56:34 +0000

purl (1.5-2) unstable; urgency=medium

  * Team upload.
  * Drop pypy support.

 -- Andrey Rahmatullin <wrar@debian.org>  Sun, 11 Aug 2019 00:56:13 +0500

purl (1.5-1) unstable; urgency=low

  * New upstream release.
  * Bump debhelper compatibility and version to 12 and switch to
    debhelper-compat.
  * Bump Standards-Version to 4.4.0.
  * Remove ancient X-Python(3)-Version fields.
  * Remove Python2 support.
  * Install README in pypy-purl binary package.

 -- Michael Fladischer <fladi@debian.org>  Tue, 23 Jul 2019 10:31:29 +0200

purl (1.4-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/watch: Use https protocol

  [ Michael Fladischer ]
  * New upstream release.
  * Always use pristine-tar.
  * Bump debhelper compatibility and version to 11.
  * Bump Standards-Version to 4.1.3.
  * Enable autopkgtest-pkg-python testsuite.
  * Run wrap-and-sort -bast to reduce diff size of future changes.

 -- Michael Fladischer <fladi@debian.org>  Fri, 23 Mar 2018 22:17:05 +0100

purl (1.3.1-1) unstable; urgency=low

  * Bump Standards-Version to 4.0.0.

 -- Michael Fladischer <fladi@debian.org>  Tue, 11 Jul 2017 16:11:07 +0200

purl (1.3.1-1~exp1) unstable; urgency=low

  * New upstream release.
  * Add PyPy support package.
  * Add missing Build-Depends for dh-python.
  * Fix spelling error in long description.
  * Use https:// for copyright-format 1.0 URL.

 -- Michael Fladischer <fladi@debian.org>  Tue, 16 May 2017 14:21:58 +0200

purl (1.3-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Michael Fladischer ]
  * New upstream release.
  * Bump Standards-Version to 3.9.8.
  * Remove XS-Autobuild field as it is only used by packages in contrib
    and non-free.

 -- Michael Fladischer <fladi@debian.org>  Wed, 04 May 2016 09:59:53 +0200

purl (1.2-1) unstable; urgency=low

  * New upstream release.
  * Reformat packaging files with cme for better readability.

 -- Michael Fladischer <fladi@debian.org>  Mon, 25 Jan 2016 12:53:35 +0100

purl (1.1-2) unstable; urgency=medium

  * Add python(3)-six to Build-Depends (Closes: #802108).
  * git-dpm config

 -- Michael Fladischer <fladi@debian.org>  Mon, 19 Oct 2015 10:33:51 +0200

purl (1.1-1) unstable; urgency=medium

  * Initial release (Closes: #787107).

 -- Michael Fladischer <fladi@debian.org>  Thu, 02 Apr 2015 15:46:31 +0200
